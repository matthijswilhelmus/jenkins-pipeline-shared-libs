def call (Map vars) {
    // call funtion example: updateDeploymentConfig(
    //                              project: 'name of project where to deploy',
    //                              appName: 'the application name',
    //                              imageTag: 'The image tag'
    // )
    openshift.withCluster() {
        openshift.withProject(vars.project) {
            def dc = openshift.selector("dc", "${vars.appName}-${vars.appTag}").object()
            dc.spec.template.spec.containers[0].image = "docker-registry.default.svc:5000/${vars.project}/${vars.appName}:${vars.imageTag}"
            openshift.apply(dc)
        }
    }
}
