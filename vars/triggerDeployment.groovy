def call (Map vars) {
    // call funtion example: triggerDeployment(
    //                              project: 'name of project where to deploy',
    //                              appName: 'the application name'
    // )
    openshift.withCluster() {
        openshift.withProject(vars.project) {
            def dc = openshift.selector("dc", "${vars.appName}-${vars.appTag}")
            dc.rollout().latest()
            dc.rollout().status()
        }
    }
}
